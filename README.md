# Escape Roots

## Prerequisites

To play our game, you must have the following OS and install the required dependencies:

- Windows 8 or higher
- DirectX 11 or higher. 
    - It usually comes with your Windows updates. Please check if you have it already installed following the [next steps](https://www.lifewire.com/how-to-download-install-directx-2624489#toc-how-to-find-the-current-directx-version-number)
- [.NET Desktop Runtime 6.0.x](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
    - Not to confuse with the `.NET SDK 6.0.x`
- [Latest Visual C++ Redistributable libraries](https://aka.ms/vs/17/release/vc_redist.x64.exe)
    - You can also check the [complete list](https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170) in case you have a different architecture


# Installation and running steps

1. Go to the next [page](https://drive.google.com/drive/folders/1R5vNDRP_GydSAmaxdWiRwSJATDKH9ny-)

2. Download `EscapeRoots.zip` file 

3. Extract the downloaded .zip file

4. Go to the `EscapeRoots` folder and double click `EscapeRoots.exe` file

5. The game will start. You can use the `mouse` and `AWSD keys` from your keyboard to move the character

6. If you want to close the game, press `ESC` key
